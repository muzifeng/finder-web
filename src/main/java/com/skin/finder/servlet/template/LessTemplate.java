/*
 * $RCSfile: LessTemplate.java,v $
 * $Revision: 1.1 $
 *
 * JSP generated by JspCompiler-1.0.0
 */
package com.skin.finder.servlet.template;

import java.io.IOException;
import java.io.OutputStream;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;



/**
 * <p>Title: LessTemplate</p>
 * <p>Description: </p>
 * @author JspKit
 * @version 1.0
 */
public class LessTemplate extends com.skin.finder.web.servlet.JspServlet {
    private static final long serialVersionUID = 1L;
    private static final LessTemplate instance = new LessTemplate();


    /**
     * @param request
     * @param response
     * @throws IOException
     * @throws ServletException
     */
    public static void execute(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
        instance.service(request, response);
    }

    /**
     * @param request
     * @param response
     * @throws IOException
     * @throws ServletException
     */
    @Override
    public void service(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
        response.setContentType("text/html; charset=utf-8");
        OutputStream out = response.getOutputStream();

        out.write(_jsp_string_0, 0, _jsp_string_0.length);
        out.write(_jsp_string_1, 0, _jsp_string_1.length);
        out.write(_jsp_string_2, 0, _jsp_string_2.length);
        this.write(out, request.getAttribute("fileName"));
        out.write(_jsp_string_4, 0, _jsp_string_4.length);
        out.write(_jsp_string_5, 0, _jsp_string_5.length);
        out.write(_jsp_string_6, 0, _jsp_string_6.length);
        out.write(_jsp_string_7, 0, _jsp_string_7.length);
        out.write(_jsp_string_8, 0, _jsp_string_8.length);
        out.write(_jsp_string_9, 0, _jsp_string_9.length);
        out.write(_jsp_string_10, 0, _jsp_string_10.length);
        this.write(out, request.getAttribute("contextPath"));
        out.write(_jsp_string_12, 0, _jsp_string_12.length);
        this.write(out, request.getAttribute("host"));
        out.write(_jsp_string_14, 0, _jsp_string_14.length);
        this.write(out, request.getAttribute("workspace"));
        out.write(_jsp_string_16, 0, _jsp_string_16.length);
        this.write(out, request.getAttribute("parent"));
        out.write(_jsp_string_18, 0, _jsp_string_18.length);
        this.write(out, request.getAttribute("path"));
        out.write(_jsp_string_20, 0, _jsp_string_20.length);
        this.write(out, request.getAttribute("charset"));
        out.write(_jsp_string_22, 0, _jsp_string_22.length);
        out.write(_jsp_string_23, 0, _jsp_string_23.length);
        out.write(_jsp_string_24, 0, _jsp_string_24.length);
        out.write(_jsp_string_25, 0, _jsp_string_25.length);
        out.write(_jsp_string_26, 0, _jsp_string_26.length);
        this.write(out, request.getAttribute("host"));
        out.write(_jsp_string_28, 0, _jsp_string_28.length);
        this.write(out, request.getAttribute("workspace"));
        out.write(_jsp_string_30, 0, _jsp_string_30.length);
        this.write(out, request.getAttribute("path"));
        out.write(_jsp_string_32, 0, _jsp_string_32.length);
        this.write(out, request.getAttribute("fileName"));
        out.write(_jsp_string_34, 0, _jsp_string_34.length);
        this.write(out, request.getAttribute("charset"));
        out.write(_jsp_string_36, 0, _jsp_string_36.length);
        out.write(_jsp_string_37, 0, _jsp_string_37.length);
        out.write(_jsp_string_38, 0, _jsp_string_38.length);
        out.write(_jsp_string_39, 0, _jsp_string_39.length);

        out.flush();
    }

    protected static final byte[] _jsp_string_0 = b("<!DOCTYPE html>\r\n<html lang=\"en\">\r\n<head>\r\n<meta http-equiv=\"Content-Type\" content=\"text/html; charset=utf-8\"/>\r\n");
    protected static final byte[] _jsp_string_1 = b("<meta http-equiv=\"Pragma\" content=\"no-cache\"/>\r\n<meta http-equiv=\"Cache-Control\" content=\"no-cache\"/>\r\n");
    protected static final byte[] _jsp_string_2 = b("<meta http-equiv=\"Expires\" content=\"0\"/>\r\n<title>less - ");
    protected static final byte[] _jsp_string_4 = b(" - Powered by Finder</title>\r\n<link rel=\"shortcut icon\" href=\"?action=res&path=/finder/images/favicon.png\"/>\r\n");
    protected static final byte[] _jsp_string_5 = b("<link rel=\"stylesheet\" type=\"text/css\" href=\"?action=res&path=/finder/css/less.css\"/>\r\n");
    protected static final byte[] _jsp_string_6 = b("<script type=\"text/javascript\" src=\"?action=res&path=/finder/jquery-1.7.2.min.js\"></script>\r\n");
    protected static final byte[] _jsp_string_7 = b("<script type=\"text/javascript\" src=\"?action=res&path=/finder/config.js\"></script>\r\n");
    protected static final byte[] _jsp_string_8 = b("<script type=\"text/javascript\" src=\"?action=res&path=/finder/charset.js\"></script>\r\n");
    protected static final byte[] _jsp_string_9 = b("<script type=\"text/javascript\" src=\"?action=res&path=/finder/less.js\"></script>\r\n");
    protected static final byte[] _jsp_string_10 = b("</head>\r\n<body contextPath=\"");
    protected static final byte[] _jsp_string_12 = b("\" host=\"");
    protected static final byte[] _jsp_string_14 = b("\" workspace=\"");
    protected static final byte[] _jsp_string_16 = b("\" parent=\"");
    protected static final byte[] _jsp_string_18 = b("\" path=\"");
    protected static final byte[] _jsp_string_20 = b("\" charset=\"");
    protected static final byte[] _jsp_string_22 = b("\">\r\n<div id=\"less-container\" class=\"less-container\" contenteditable=\"true\" spellcheck=\"false\"></div>\r\n");
    protected static final byte[] _jsp_string_23 = b("<div id=\"less-progress-bar\" class=\"less-progress-bar\">\r\n    <div class=\"progress\">\r\n");
    protected static final byte[] _jsp_string_24 = b("        <div class=\"slider\">\r\n            <div class=\"pace\"></div>\r\n            <a class=\"dot\" href=\"#\"></a>\r\n");
    protected static final byte[] _jsp_string_25 = b("            <div class=\"mask\"></div>\r\n        </div>\r\n    </div>\r\n</div>\r\n<div id=\"less-status-bar\" class=\"less-status-bar\">\r\n");
    protected static final byte[] _jsp_string_26 = b("    <div style=\"height: 18px; background-color: #333333;\">\r\n        <span class=\"file\"><input id=\"less-file\" type=\"text\" class=\"text w240\" readonly=\"true\" title=\"");
    protected static final byte[] _jsp_string_28 = b("@");
    protected static final byte[] _jsp_string_30 = b("/");
    protected static final byte[] _jsp_string_32 = b("\" value=\"");
    protected static final byte[] _jsp_string_34 = b("\"/></span>\r\n        <span class=\"charset\"><select name=\"charset\" selected-value=\"");
    protected static final byte[] _jsp_string_36 = b("\"></select></span>\r\n        <span class=\"info\"><input id=\"less-info\" type=\"text\" class=\"text w160\" value=\"0 B\"/></span>\r\n");
    protected static final byte[] _jsp_string_37 = b("        <span class=\"status\"><input id=\"less-status\" type=\"text\" class=\"text w160\" readonly=\"true\" value=\"READY\"/></span>\r\n");
    protected static final byte[] _jsp_string_38 = b("    </div>\r\n</div>\r\n<div id=\"less-tooltip\" class=\"less-tooltip\">50%</div>\r\n<!-- http://www.finderweb.net -->\r\n");
    protected static final byte[] _jsp_string_39 = b("</body>\r\n</html>\r\n");

}
