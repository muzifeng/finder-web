/*
 * $RCSfile: LogChatTemplate.java,v $
 * $Revision: 1.1 $
 *
 * JSP generated by JspCompiler-1.0.0
 */
package com.skin.finder.servlet.template;

import java.io.IOException;
import java.io.OutputStream;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;



/**
 * <p>Title: LogChatTemplate</p>
 * <p>Description: </p>
 * @author JspKit
 * @version 1.0
 */
public class LogChatTemplate extends com.skin.finder.web.servlet.JspServlet {
    private static final long serialVersionUID = 1L;
    private static final LogChatTemplate instance = new LogChatTemplate();


    /**
     * @param request
     * @param response
     * @throws IOException
     * @throws ServletException
     */
    public static void execute(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
        instance.service(request, response);
    }

    /**
     * @param request
     * @param response
     * @throws IOException
     * @throws ServletException
     */
    @Override
    public void service(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
        response.setContentType("text/html; charset=utf-8");
        OutputStream out = response.getOutputStream();

        out.write(_jsp_string_1, 0, _jsp_string_1.length);
        out.write(_jsp_string_2, 0, _jsp_string_2.length);
        out.write(_jsp_string_3, 0, _jsp_string_3.length);
        out.write(_jsp_string_4, 0, _jsp_string_4.length);
        out.write(_jsp_string_5, 0, _jsp_string_5.length);
        out.write(_jsp_string_6, 0, _jsp_string_6.length);
        this.write(out, request.getAttribute("contextPath"));
        out.write(_jsp_string_8, 0, _jsp_string_8.length);
        this.write(out, request.getAttribute("workspace"));
        out.write(_jsp_string_10, 0, _jsp_string_10.length);
        out.write(_jsp_string_11, 0, _jsp_string_11.length);
        out.write(_jsp_string_12, 0, _jsp_string_12.length);
        out.write(_jsp_string_13, 0, _jsp_string_13.length);
        out.write(_jsp_string_14, 0, _jsp_string_14.length);
        out.write(_jsp_string_15, 0, _jsp_string_15.length);
        out.write(_jsp_string_16, 0, _jsp_string_16.length);
        out.write(_jsp_string_17, 0, _jsp_string_17.length);

        out.flush();
    }

    protected static final byte[] _jsp_string_1 = b("<!DOCTYPE html>\r\n<html lang=\"en\">\r\n<head>\r\n<meta http-equiv=\"Content-Type\" content=\"text/html; charset=utf-8\"/>\r\n");
    protected static final byte[] _jsp_string_2 = b("<meta http-equiv=\"Pragma\" content=\"no-cache\"/>\r\n<meta http-equiv=\"Cache-Control\" content=\"no-cache\"/>\r\n");
    protected static final byte[] _jsp_string_3 = b("<meta http-equiv=\"Expires\" content=\"0\"/>\r\n<title>Finder - Powered by Finder</title>\r\n");
    protected static final byte[] _jsp_string_4 = b("<link rel=\"stylesheet\" type=\"text/css\" href=\"?action=res&path=/finder/css/form.css\"/>\r\n");
    protected static final byte[] _jsp_string_5 = b("<script type=\"text/javascript\" src=\"?action=res&path=/finder/jquery-1.7.2.min.js\"></script>\r\n");
    protected static final byte[] _jsp_string_6 = b("</head>\r\n<body contextPath=\"");
    protected static final byte[] _jsp_string_8 = b("\" workspace=\"");
    protected static final byte[] _jsp_string_10 = b("\">\r\n<div class=\"box-wrap\">\r\n    <div id=\"finder-panel\" class=\"form\">\r\n        <div class=\"menu-panel\"><h4>Hello</h4></div>\r\n");
    protected static final byte[] _jsp_string_11 = b("        <div class=\"form-row\">\r\n            <div class=\"form-label\">昵称：</div>\r\n            <div class=\"form-c300\">\r\n");
    protected static final byte[] _jsp_string_12 = b("                <div class=\"form-field\">\r\n                    \r\n                </div>\r\n");
    protected static final byte[] _jsp_string_13 = b("            </div>\r\n        </div>\r\n        <div class=\"form-row\">\r\n            <div class=\"form-label\">内容：</div>\r\n");
    protected static final byte[] _jsp_string_14 = b("            <div class=\"form-c300\">\r\n                <div class=\"form-field\">\r\n                    <textarea name=\"message\"></textarea>\r\n");
    protected static final byte[] _jsp_string_15 = b("                </div>\r\n            </div>\r\n        </div>\r\n        <div class=\"button\">\r\n");
    protected static final byte[] _jsp_string_16 = b("            <button id=\"ensure-btn\" class=\"button ensure\">发 送</button>\r\n        </div>\r\n");
    protected static final byte[] _jsp_string_17 = b("    </div>\r\n</div>\r\n</body>\r\n</html>\r\n");

}
